package com.example.recyclerviewscreendemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @功能:
 * @Creat 2020/3/11 14:16
 * @User Lmy
 * @Compony JinAnChang
 */
public class ScreenAdapter extends RecyclerView.Adapter<ScreenAdapter.ViewHolder> implements ScreenItemAdapter.OnItemClickListener {
    //新增itemType
    public Context context;
    private List<MyClientScreenBean.DataBean> resultDataBeans;
    private ScreenItemAdapter mRvAdapter;
    private Map<String, String> danxuan = new HashMap<>();
    private Map<String, List<String>> duoxuan = new HashMap<>();

    public ScreenAdapter(Context context, List<MyClientScreenBean.DataBean> resultDataBeans) {
        this.context = context;
        this.resultDataBeans = resultDataBeans;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.sereen_recyclerview, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvTitle.setText(resultDataBeans.get(position).getTitle());
        mRvAdapter = new ScreenItemAdapter(context, resultDataBeans.get(position).getOption(), resultDataBeans.get(position).getType(), resultDataBeans.get(position).getKey());
        mRvAdapter.setOnItemClickListener(this);
        GridLayoutManager layoutManage = new GridLayoutManager(context, 4);
        holder.recyclerview.setLayoutManager(layoutManage);
        holder.recyclerview.setLayoutFrozen(true);
        holder.recyclerview.setAdapter(mRvAdapter);
    }

    @Override
    public int getItemCount() {
        return resultDataBeans.size();
    }

    @Override
    public void onItemDanxuanClick(String name, String value) {
        danxuan.put(name, value);
        mOnItemClickListener.onItemDanxuanClick(danxuan);
    }

    @Override
    public void onItemDuoxuanClick(String name, List<String> value) {
        duoxuan.put(name, value);
        mOnItemClickListener.onItemDuoxuanClick(duoxuan);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private RecyclerView recyclerview;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            recyclerview = itemView.findViewById(R.id.recyclerview);

        }
    }

    public static OnItemClickListener mOnItemClickListener;

    //define interface 自定义一个接口
    public static interface OnItemClickListener {
        void onItemDanxuanClick(Map<String, String> name);

        void onItemDuoxuanClick(Map<String, List<String>> duoxuan);
    }

    //set方法：
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
}
