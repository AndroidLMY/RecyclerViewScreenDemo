package com.example.recyclerviewscreendemo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements ScreenAdapter.OnItemClickListener {
    public ScreenAdapter screenAdapter;
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
    private RecyclerView recyclerview;

    private Map<String, String> danxuan = new HashMap<>();
    private List<Map<String, List<String>>> duoxaun = new ArrayList<>();

    private String data = "{\n" +
            "    \"result\": \"200\",\n" +
            "    \"errorMsg\": \"\",\n" +
            "    \"errorCode\": \"500\",\n" +
            "    \"data\": [\n" +
            "        {\n" +
            "            \"title\": \"客户状态\",\n" +
            "            \"key\": \"state\",\n" +
            "            \"type\": \"1\",\n" +
            "            \"option\": [\n" +
            "                {\n" +
            "                    \"name\": \"A\",\n" +
            "                    \"id\": \"1\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"name\": \"B\",\n" +
            "                    \"id\": \"1\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"name\": \"C\",\n" +
            "                    \"id\": \"1\"\n" +
            "                }\n" +
            "            ]\n" +
            "        },{\n" +
            "            \"title\": \"客户状态\",\n" +
            "            \"key\": \"state2\",\n" +
            "            \"type\": \"1\",\n" +
            "            \"option\": [\n" +
            "                {\n" +
            "                    \"name\": \"A\",\n" +
            "                    \"id\": \"1\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"name\": \"B\",\n" +
            "                    \"id\": \"1\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"name\": \"C\",\n" +
            "                    \"id\": \"1\"\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"title\": \"客户渠道\",\n" +
            "            \"key\": \"channel\",\n" +
            "            \"type\": \"2\",\n" +
            "            \"option\": [\n" +
            "                {\n" +
            "                    \"name\": \"A\",\n" +
            "                    \"id\": \"1\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"name\": \"B\",\n" +
            "                    \"id\": \"1\"\n" +
            "                },\n" +
            "                {\n" +
            "                    \"name\": \"C\",\n" +
            "                    \"id\": \"1\"\n" +
            "                }\n" +
            "            ]\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Gson gson = new Gson();
        MyClientScreenBean houseAddressBean = gson.fromJson(data, MyClientScreenBean.class);
        recyclerview = findViewById(R.id.recyclerview);
        screenAdapter = new ScreenAdapter(this, houseAddressBean.getData());
        screenAdapter.setOnItemClickListener(this);
        recyclerview.setLayoutManager(linearLayoutManager);
        recyclerview.setAdapter(screenAdapter);
    }

    @Override
    public void onItemDanxuanClick(Map<String, String> map) {
        /**
         * 便利map
         */
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String mapKey = entry.getKey();
            String mapValue = entry.getValue();
        }
        Log.e("TTTTTTT", "" + map.toString());
    }
    @Override
    public void onItemDuoxuanClick(Map<String, List<String>> duoxuan) {
        Log.e("TTTTTTT", "" + duoxuan.toString());

    }
}
