package com.example.recyclerviewscreendemo;

import java.util.List;

/**
 * @功能:筛选区域数据
 * @Creat 2020/1/15 9:03
 * @User Lmy
 * @Compony JinAnChang
 */
public class MyClientScreenBean {


    /**
     * result : 200
     * errorMsg :
     * errorCode : 500
     * data : [{"title":"客户状态","key":"state","type":"1","option":[{"name":"A","id":"1"},{"name":"B","id":"1"},{"name":"C","id":"1"}]}]
     */

    private String result;
    private String errorMsg;
    private String errorCode;
    private List<DataBean> data;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * title : 客户状态
         * key : state
         * type : 1
         * option : [{"name":"A","id":"1"},{"name":"B","id":"1"},{"name":"C","id":"1"}]
         */

        private String title;
        private String key;
        private String type;
        private List<OptionBean> option;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<OptionBean> getOption() {
            return option;
        }

        public void setOption(List<OptionBean> option) {
            this.option = option;
        }

        public static class OptionBean {
            /**
             * name : A
             * id : 1
             */

            private boolean isclick;

            public boolean isIsclick() {
                return isclick;
            }

            public void setIsclick(boolean isclick) {
                this.isclick = isclick;
            }

            private String name;
            private String id;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }
    }
}
