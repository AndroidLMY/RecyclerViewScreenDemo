package com.example.recyclerviewscreendemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * @功能:
 * @Creat 2020/3/11 15:45
 * @User Lmy
 * @Compony JinAnChang
 */
public class ScreenItemAdapter extends RecyclerView.Adapter<ScreenItemAdapter.ViewHolder> {
    public Context context;
    private List<MyClientScreenBean.DataBean.OptionBean> optionBeanList;
    public String type;
    private boolean danshaung;
    private String key;

    public ScreenItemAdapter(Context context, List<MyClientScreenBean.DataBean.OptionBean> optionBeanList, String type, String key) {
        this.context = context;
        this.optionBeanList = optionBeanList;
        this.type = type;
        this.key = key;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.sereen_recyclerview_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.tvname.setText(optionBeanList.get(position).getName());
        holder.itemView.setTag(position);

        //单选
        if (optionBeanList.get(position).isIsclick()) {
            holder.tvname.setBackground(context.getResources().getDrawable(R.drawable.area_sure));
        } else {
            holder.tvname.setBackground(context.getResources().getDrawable(R.drawable.area_reset));
        }
        holder.tvname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("1")) {
                    //单选
                    for (int i = 0; i < optionBeanList.size(); i++) {
                        optionBeanList.get(i).setIsclick(false);
                    }
                    optionBeanList.get(position).setIsclick(true);
                    mOnItemClickListener.onItemDanxuanClick(key, optionBeanList.get(position).getId());
                } else {
                    List<String> duoxuan = new ArrayList<>();
                    //多选
                    if (optionBeanList.get(position).isIsclick()) {
                        optionBeanList.get(position).setIsclick(false);
                    } else {
                        optionBeanList.get(position).setIsclick(true);
                    }

                    for (int i = 0; i < optionBeanList.size(); i++) {
                        if (optionBeanList.get(i).isIsclick()) {
                            duoxuan.add(optionBeanList.get(i).getId());
                        }
                    }
                    mOnItemClickListener.onItemDuoxuanClick(key, duoxuan);


                }
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return optionBeanList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvname;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvname = itemView.findViewById(R.id.name);
        }
    }

    public static OnItemClickListener mOnItemClickListener;

    //define interface 自定义一个接口
    public static interface OnItemClickListener {
        void onItemDanxuanClick(String name, String value);

        void onItemDuoxuanClick(String name, List<String> value);

    }

    //set方法：
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
}
